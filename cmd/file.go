package cmd

import (
	"github.com/spf13/cobra"
	"secret-resolver/pkg/aws"
)

var fileResolveCommand = &cobra.Command{
	Use:   "file",
	Short: "Resolves secrets in a given file",
	Long:  "Resolves secrets in a given file",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		resolver := aws.NewSecretResolver()
		secret, err := resolver.GetSecret(args[0])
		cobra.CheckErr(err)
	},
}

func init() {
	rootCommand.AddCommand(fileResolveCommand)
}
