package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"secret-resolver/pkg/aws"
)

var parameterResolveCommand = &cobra.Command{
	Use:   "parameter",
	Short: "Resolve secret with parameter",
	Long:  "Resolve a given secret and return the given parameter. Instance tags can be appended by setting them between [ and ]",
	Args:  cobra.ExactArgs(2),
	Run: func(cmd *cobra.Command, args []string) {
		secret := args[0]
		parameter := args[1]

		resolver := aws.NewSecretResolver()
		secret, err := resolver.GetParameter(secret, parameter)
		cobra.CheckErr(err)

		fmt.Println(secret)
	},
}

func init() {
	rootCommand.AddCommand(parameterResolveCommand)
}
