package cmd

import "github.com/spf13/cobra"

var Region string

var rootCommand = &cobra.Command{
	Use:   "ready-secret",
	Short: "Ready Secret Resolver",
	Long:  `Multiple command line option for dealing with secrets and tags`,
	Run: func(cmd *cobra.Command, args []string) {
		err := cmd.Help()
		cobra.CheckErr(err)
	},
}

func Execute() {
	cobra.CheckErr(rootCommand.Execute())
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCommand.PersistentFlags().StringVarP(&Region, "region", "r", "eu-east-1", "aws region that is used")

}

func initConfig() {

}
