package cmd

import (
	"encoding/json"
	"fmt"
	"github.com/spf13/cobra"
	"secret-resolver/pkg/aws"
)

var prettyPrint bool

var secretResolveCommand = &cobra.Command{
	Use:   "secret",
	Short: "Resolve secret",
	Long:  "Resolve a given secret",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		resolver := aws.NewSecretResolver()
		secret, err := resolver.GetSecret(args[0])
		cobra.CheckErr(err)

		var data []byte
		if prettyPrint {
			data, err = json.MarshalIndent(secret, "", "    ")
		} else {
			data, err = json.Marshal(secret)
		}
		cobra.CheckErr(err)
		fmt.Println(string(data))
	},
}

func init() {
	secretResolveCommand.Flags().BoolVarP(&prettyPrint, "pretty", "p", false, "pretty print the json")
	rootCommand.AddCommand(secretResolveCommand)
}
