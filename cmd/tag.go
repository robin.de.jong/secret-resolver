package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"secret-resolver/pkg/aws"
)

var tagResolveCommand = &cobra.Command{
	Use:   "tags",
	Short: "List all tags",
	Long:  "List all the tags that are given to the current EC2 instance",
	Run: func(cmd *cobra.Command, args []string) {
		tag := aws.NewTagManager()
		tags := tag.GetTags()

		for key, value := range tags {
			fmt.Printf("[%s] = %s\n", key, value)
		}
	},
}

func init() {
	rootCommand.AddCommand(tagResolveCommand)
}
