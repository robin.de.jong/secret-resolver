package aws

import (
	"context"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/ec2"
	"github.com/aws/aws-sdk-go-v2/service/ec2/types"
	"github.com/spf13/cobra"
	"log"
)

type InstanceManager struct {
	awsEc2 *ec2.Client
}

func NewInstanceManager() *InstanceManager {
	instance := new(InstanceManager)
	cfg, err := config.LoadDefaultConfig(context.TODO(), config.WithRegion("us-east-1"))
	if err != nil {
		log.Fatalf("Unable to load SDK config %v", err)
	}
	instance.awsEc2 = ec2.NewFromConfig(cfg)

	return instance
}

func (i *InstanceManager) GetInstanceDetails(instance string, token string) []types.Tag {
	input := &ec2.DescribeInstancesInput{
		InstanceIds: []string{instance},
	}
	out, err := i.awsEc2.DescribeInstances(context.TODO(), input)
	cobra.CheckErr(err)
	return out.Reservations[0].Instances[0].Tags
}
