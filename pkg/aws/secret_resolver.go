package aws

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/secretsmanager"
	"log"
	"strings"
)

type SecretResolver struct {
	awsSecretsManager *secretsmanager.Client
	tagManager *TagManager
}

func NewSecretResolver() *SecretResolver {
	resolver := new(SecretResolver)

	cfg, err := config.LoadDefaultConfig(context.TODO(), config.WithRegion("us-east-1"))
	if err != nil {
		log.Fatalf("Unable to load SDK config %v", err)
	}

	resolver.awsSecretsManager = secretsmanager.NewFromConfig(cfg)
	resolver.tagManager = NewTagManager()

	return resolver
}

func (r *SecretResolver) normalizeSecretInput(input string) string {
	tags := r.tagManager.GetTags()

	for key, value := range tags {
		tag := fmt.Sprint("[", key, "]")
		input = strings.ReplaceAll(input, tag, value)
	}

	return input
}

func (r *SecretResolver) GetSecret(secret string) (map[string] string, error) {
	secret = r.normalizeSecretInput(secret)

	input := &secretsmanager.GetSecretValueInput{
		SecretId: aws.String(secret),
	}
	result, err := r.awsSecretsManager.GetSecretValue(context.TODO(), input)
	if err != nil {
		log.Printf("Unable to get secret %v", err)
		return nil, err
	}

	version := map[string]string{}
	err = json.Unmarshal([]byte(*result.SecretString), &version)
	if err != nil {
		log.Printf("Unable to marshal secret: %v", err)
		return nil, err
	}

	return version, nil
}

func (r *SecretResolver) GetParameter(secret string, parameter string) (string, error) {
	version, err := r.GetSecret(secret)
	if err != nil {
		log.Printf("Unable to get secret: %v", err)
		return "", err
	}

	return version[parameter], nil
}
