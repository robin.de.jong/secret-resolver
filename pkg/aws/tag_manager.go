package aws

import (
	"github.com/spf13/cobra"
	"io"
	"net/http"
)

const DetailUrl = "http://169.254.169.254/latest"

type TagManager struct {
	httpClient *http.Client
	instanceManager *InstanceManager
}

func NewTagManager() *TagManager {
	tag := new(TagManager)
	tag.httpClient = &http.Client{}
	tag.instanceManager = NewInstanceManager()
	return tag
}

func (t *TagManager) getToken() string {
	req, err := http.NewRequest(http.MethodPut, DetailUrl+"/api/token", nil)
	cobra.CheckErr(err)
	req.Header.Set("X-aws-ec2-metadata-token-ttl-seconds", "21600")
	response, err := t.httpClient.Do(req)
	cobra.CheckErr(err)

	bytes, err := io.ReadAll(response.Body)
	cobra.CheckErr(err)

	return string(bytes)
}

func (t *TagManager) getRegion(token string) string {
	req, err := http.NewRequest(http.MethodGet, DetailUrl + "/meta-data/instance-id", nil)
	cobra.CheckErr(err)
	req.Header.Set("X-aws-ec2-metadata-token", token)

	response, err := t.httpClient.Do(req)
	cobra.CheckErr(err)

	bytes, err := io.ReadAll(response.Body)
	cobra.CheckErr(err)

	return string(bytes)
}

func (t *TagManager) getInstanceId(token string) string {
	req, err := http.NewRequest(http.MethodGet, DetailUrl + "/meta-data/instance-id", nil)
	cobra.CheckErr(err)
	req.Header.Set("X-aws-ec2-metadata-token", token)

	response, err := t.httpClient.Do(req)
	cobra.CheckErr(err)

	bytes, err := io.ReadAll(response.Body)
	cobra.CheckErr(err)

	return string(bytes)
}

func (t *TagManager) getTags(instanceId string, token string) map[string] string {
	tags := t.instanceManager.GetInstanceDetails(instanceId, token)

	newTags := make(map[string] string)

	for _, tag := range tags {
		newTags[*tag.Key] = *tag.Value
	}

	return newTags
}

func (t *TagManager) GetTags() map[string] string {
	token := t.getToken()
	instanceId := t.getInstanceId(token)
	return t.getTags(instanceId, token)
}
